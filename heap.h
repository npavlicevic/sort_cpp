#ifndef HEAP_H
#define HEAP_H
#include "util.h"
class Heap {
  public:
    Heap(int capacity_);
    ~Heap();
    void push(int item, Util *util);
    int pop(Util *util);
    void sort(int *items_, int n, Util *util);
  protected:
    int position;
    int capacity;
    int *items;
};
#endif
