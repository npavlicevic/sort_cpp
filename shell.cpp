#include "shell.h"
void Shell::sort(int *items, int n, Insert *insert, Util *util) {
  int ngaps = 8;
  int gaps[] = {23, 19, 17, 11, 7, 5, 3, 1};
  for(int i = 0; i < ngaps; i++) {
    insert->sort(items, n, gaps[i], util);
  }
}
