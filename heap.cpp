#include "heap.h"

Heap::Heap(int capacity_) {
  position = 1;
  capacity = capacity_;
  items = new int[capacity];
}
Heap::~Heap() {
  delete items;
}
void Heap::push(int item, Util *util) {
  items[position] = item;
  int child = position;
  int parent = child / 2;
  for(;parent > 0;) {
    if(items[child] < items[parent]) {
      util->array_swap(items, child, parent);
      child = parent;
      parent /= 2;
    } else {
      break;
    }
  }
  position++;
}
int Heap::pop(Util *util) {
  int r = items[1];
  items[1] = items[--position];
  int parent = 1;
  int left_child;
  int right_child;
  int direction;
  // 0 left 1 right
  for(;1;) {
    left_child = 2*parent;
    right_child = 2*parent+1;
    if(left_child >= position && right_child >= position) {
      break;
    }
    if(right_child >= position) {
      direction = 0;
    } else if(left_child >= position) {
      direction = 1;
    } else if(items[left_child] < items[right_child]) {
      direction = 0;
    } else {
      direction = 1;
    }
    if(!direction) {
      if(items[left_child] < items[parent]) {
        util->array_swap(items, left_child, parent);
        parent = left_child;
      } else {
        break;
      }
    } else {
      if(items[right_child] < items[parent]) {
        util->array_swap(items, right_child, parent);
        parent = right_child;
      } 
      else {
        break;
      }
    }
  }
  return r;
}
void Heap::sort(int *items_, int n, Util *util) {
  int i = 0;
  for(; i < n; i++) {
    push(items_[i], util);
  }
  for(i = 0; i < n; i++) {
    items_[i] = pop(util);
  }
}
