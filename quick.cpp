#include "quick.h"

int Quick::split_boundries(int *items, int lo, int hi, Util *util) {
  int item = items[hi];
  int lo_ = lo;
  int hi_ = hi - 1;
  for(;lo_ <= hi_;) {
    for(;items[lo_] < item;) {
      lo_++;
    }
    for(;items[hi_] > item;) {
      hi_--;
    }
    if(lo_ <= hi_) {
      util->array_swap(items, lo_, hi_);
      lo_++;
      hi_--;
    }
  }
  util->array_swap(items, lo_, hi);
  return lo_;
}
int Quick::split(int *items, int lo, int hi, Util *util) {
  int mid = (lo + hi) / 2;
  int pivot_element = items[mid];
  util->array_swap(items, mid, hi);
  int i = lo, j = lo;
  for(; i < hi; i++) {
    if(items[i] <= pivot_element) {
      util->array_swap(items, i, j);
      j++;
    }
  }
  util->array_swap(items, j, hi);
  return j;
}
void Quick::sort(int *items, int lo, int hi, Util *util) {
  if(hi > lo) {
    int mid_ = split(items, lo, hi, util);
    sort(items, lo, mid_ - 1, util);
    sort(items, mid_ + 1, hi, util);
  }
}
void Quick::sort_boundries(int *items, int lo, int hi, Util *util) {
  if(hi > lo) {
    int mid_ = split_boundries(items, lo, hi, util);
    sort_boundries(items, lo, mid_ - 1, util);
    sort_boundries(items, mid_ + 1, hi, util);
  }
}
void Quick::sort_double_stack(int *items, int lo, int hi, Util *util) {
  int stack_len = 16384;
  int lo__;
  int lo_ = 0;
  int lo_stack[stack_len];
  lo_stack[lo_++] = lo;
  int hi__;
  int hi_ = 0;
  int hi_stack[stack_len];
  hi_stack[hi_++] = hi;
  int mid;
  for(;lo_ > 0 && hi_ > 0;) {
    lo__ = lo_stack[--lo_];
    hi__ = hi_stack[--hi_];
    if(hi__ < lo__) continue;
    mid = split_boundries(items, lo__, hi__, util);
    lo_stack[lo_++] = lo__;
    hi_stack[hi_++] = mid-1;
    lo_stack[lo_++] = mid+1;
    hi_stack[hi_++] = hi__;
  }
}
