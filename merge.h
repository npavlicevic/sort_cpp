#ifndef MERGE_H
#define MERGE_H
#include "util.h"
#include <time.h>
class Merge {
  public:
    void merge(int *merged, int *items, int lo, int mid, int hi);
    void sort(int *items, int *merged, int lo, int hi, Util *util);
    void copy(int *items, int *merged, int lo, int hi, Util *util);
};
#endif
