#ifndef COMB_H
#define COMB_H
#include "util.h"
class Comb {
  public:
    void sort(int *items, int n, int gap, Util *util);
};
#endif
