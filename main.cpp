#include "bubble.h"

int main() {
  Util *util = new Util();
  Bubble *bubble = new Bubble();
  int n;
  fscanf(stdin, "%d", &n);
  int *items = new int[n];
  util->array_init(items, n);
  util->array_print(items, n);
  bubble->sort(items, n, 1, util);
  util->array_print(items, n);
  delete util;
  delete bubble;
  delete items;
  return 0;
}
