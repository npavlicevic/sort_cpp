#ifndef BUBBLE_H
#define BUBBLE_H
#include "util.h"
class Bubble {
  public:
    void sort(int *items, int n, int gap, Util *util);
};
#endif
