#include "insert.h"
void Insert::sort(int *items, int n, int gap, Util *util) {
  for(int i = gap; i < n; i++) {
    for(int j = i; j > 0; j--) {
      if(items[j-1] > items[j]) {
        util->array_swap(items, j - 1, j);
      }
    }
  }
}
