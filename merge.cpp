#include "merge.h"
void Merge::merge(int *merged, int *items, int lo, int mid, int hi) {
  int i = lo, merged_ = lo, j = mid;
  for(;1;) {
    if(items[i] < items[j]) {
      merged[merged_++] = items[i++];
    } else {
      merged[merged_++] = items[j++];
    }
    if(i >= mid) break;
    if(j >= hi) break;
  }
  for(; i < mid; i++) {
    merged[merged_++] = items[i];
  }
  for(;j < hi; j++) {
    merged[merged_++] = items[j];
  }
}
void Merge::sort(int *items, int *merged, int lo, int hi, Util *util) {
  if(hi - lo >= 2) {
    int mid = (lo + hi)/2;
    sort(items, merged, lo, mid, util);
    sort(items, merged, mid, hi, util);
    merge(merged, items, lo, mid, hi);
    copy(items, merged, lo, hi, util);
  }
}
void Merge::copy(int *items, int *merged, int lo, int hi, Util *util) {
  util->array_copy(items, merged, lo, hi);
}
