#include "bubble.h"
void Bubble::sort(int *items, int n, int gap, Util *util) {
  int swaps = 1;
  while(swaps) {
    swaps = 0;
    for(int i = 0, j = gap; j < n; i++, j++) {
      if(items[i] > items[j]) {
        util->array_swap(items, i, j);
        swaps = 1;
      }
    }
  }
}
