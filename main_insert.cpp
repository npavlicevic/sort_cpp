#include "insert.h"

int main() {
  Util *util = new Util();
  Insert *insert = new Insert();
  int n;
  fscanf(stdin, "%d", &n);
  int *items = new int[n];
  util->array_init(items, n);
  util->array_print(items, n);
  insert->sort(items, n, 1, util);
  util->array_print(items, n);
  delete util;
  delete insert;
  delete items;
  return 0;
}
