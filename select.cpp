#include "select.h"

void Select::sort(int *items, int n, Util *util) {
  int i = 0, j, min;
  for(; i < n-1; i++) {
    min = i;
    for(j=i+1; j < n; j++) {
      if(items[j] < items[min]) {
        min = j;
      }
    }
    if(min != i) {
      util->array_swap(items, i, min);
    }
  }
}
