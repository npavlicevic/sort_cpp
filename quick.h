#ifndef QUICK_H
#define QUICK_H
#include <time.h>
#include "util.h"
class Quick {
  public:
    int split_boundries(int *items, int lo, int hi, Util *util);
    int split(int *items, int lo, int hi, Util *util);
    void sort(int *items, int lo, int hi, Util *util);
    void sort_boundries(int *items, int lo, int hi, Util *util);
    void sort_double_stack(int *items, int lo, int hi, Util *util);
};
#endif
