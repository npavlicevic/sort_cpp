#include "select.h"

int main() {
  Util *util = new Util();
  Select *select = new Select();
  int n;
  fscanf(stdin, "%d", &n);
  int *items = new int[n];
  util->array_init(items, n);
  util->array_print(items, n);
  select->sort(items, n, util);
  util->array_print(items, n);
  delete util;
  delete select;
  delete items;
  return 0;
}
