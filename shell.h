#ifndef SHELL_H
#define SHELL_H
#include "insert.h"
class Shell {
  public:
    void sort(int *items, int n, Insert *insert, Util *util);
};
#endif
