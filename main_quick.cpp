#include "quick.h"

int main() {
  Util *util = new Util();
  Quick *quick = new Quick();
  srand(time(NULL));
  int n;
  fscanf(stdin, "%d", &n);
  int *items = new int[n];
  util->array_init(items, n);
  util->array_print(items, n);
  quick->sort(items, 0, n-1, util);
  util->array_print(items, n);
  delete util;
  delete quick;
  delete items;
  return 0;
}
