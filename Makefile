#
# Makefile 
# sort
#

CC=g++
CFLAGS=-g -Wall
LIBS=-lcpparrayutil

FILES=bubble.cpp main.cpp
FILES_INSERT=insert.cpp main_insert.cpp
FILES_SHELL=insert.cpp shell.cpp main_shell.cpp
FILES_COMB=comb.cpp main_comb.cpp
FILES_QUICK=quick.cpp main_quick.cpp
FILES_QUICK_BOUNDRIES=quick.cpp main_quick_boundries.cpp
FILES_QUICK_DOUBLE_STACK=quick.cpp main_quick_double_stack.cpp
FILES_MERGE=merge.cpp main_merge.cpp
FILES_HEAP=heap.cpp main_heap.cpp
FILES_INSERT=insert.cpp main_insert.cpp
FILES_SELECT=select.cpp main_select.cpp
CLEAN=main main_insert main_shell main_comb main_quick main_quick_boundries main_quick_double_stack main_merge main_heap main_insert main_select

all: main main_insert main_shell main_comb main_quick main_quick_boundries main_quick_double_stack main_merge main_heap main_insert main_select

main: ${FILES}
	${CC} ${CFLAGS} $^ -o main ${LIBS}

main_insert: ${FILES_INSERT}
	${CC} ${CFLAGS} $^ -o main_insert ${LIBS}

main_shell: ${FILES_SHELL}
	${CC} ${CFLAGS} $^ -o main_shell ${LIBS}

main_comb: ${FILES_COMB}
	${CC} ${CFLAGS} $^ -o main_comb ${LIBS}

main_quick: ${FILES_QUICK}
	${CC} ${CFLAGS} $^ -o main_quick ${LIBS}

main_quick_boundries: ${FILES_QUICK_BOUNDRIES}
	${CC} ${CFLAGS} $^ -o main_quick_boundries ${LIBS}

main_quick_double_stack: ${FILES_QUICK_DOUBLE_STACK}
	${CC} ${CFLAGS} $^ -o main_quick_double_stack ${LIBS}

main_merge: ${FILES_MERGE}
	${CC} ${CFLAGS} $^ -o main_merge ${LIBS}

main_heap: ${FILES_HEAP}
	${CC} ${CFLAGS} $^ -o main_heap ${LIBS}

main_select: ${FILES_SELECT}
	${CC} ${CFLAGS} $^ -o main_select ${LIBS}

clean: ${CLEAN}
	rm $^
