#include "heap.h"

int main() {
  Util *util = new Util();
  int n;
  fscanf(stdin, "%d", &n);
  Heap *heap = new Heap(n+1);
  int *items = new int[n];
  util->array_init(items, n);
  util->array_print(items, n);
  heap->sort(items, n, util);
  util->array_print(items, n);
  delete util;
  delete heap;
  delete items;
  return 0;
}
