#include "comb.h"

int main() {
  Util *util = new Util();
  Comb *comb = new Comb();
  int n;
  fscanf(stdin, "%d", &n);
  int *items = new int[n];
  util->array_init(items, n);
  util->array_print(items, n);
  comb->sort(items, n, n-1, util);
  util->array_print(items, n);
  delete util;
  delete comb;
  delete items;
  return 0;
}
