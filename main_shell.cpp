#include "shell.h"

int main() {
  Util *util = new Util();
  Insert *insert = new Insert();
  Shell *shell = new Shell();
  int n;
  fscanf(stdin, "%d", &n);
  int *items = new int[n];
  util->array_init(items, n);
  util->array_print(items, n);
  shell->sort(items, n, insert, util);
  util->array_print(items, n);
  delete util;
  delete insert;
  delete shell;
  delete items;
  return 0;
}
