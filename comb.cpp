#include "comb.h"
void Comb::sort(int *items, int n, int gap, Util *util) {
  int swaps = 1;
  double factor = 1.2;
  while(gap || swaps) {
    swaps = 0;
    for(int i = 0, j = gap; j < n; i++, j++) {
      if(items[i] > items[j]) {
        util->array_swap(items, i, j);
        swaps = 1;
      }
    }
    gap /= factor;
  }
}
