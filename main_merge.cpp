#include "merge.h"

int main() {
  Util *util = new Util();
  Merge *merge = new Merge();
  srand(time(NULL));
  int n;
  fscanf(stdin, "%d", &n);
  int *items = new int[n];
  int *merged = new int[n];
  util->array_init(items, n);
  util->array_print(items, n);
  merge->sort(items, merged, 0, n, util);
  util->array_print(items, n);
  delete util;
  delete merge;
  delete items;
  delete merged;
  return 0;
}
